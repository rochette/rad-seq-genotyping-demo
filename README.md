The scripts and files in this repository accompany our Nature Protocols
article:

    Rochette NC, Catchen JM (2017) Deriving genotypes from RAD-seq short-read
    data using Stacks. Nat Protoc, 12, 2640–2659. doi.org/10.1038/nprot.2017.123

The scripts in the `demo_scripts/` and `demo_scripts/R_scripts/` directories 
demonstrate how to perform each step of the procedure on the demonstration 
dataset (which is composed of 78 Oregon Sticklebacks).

The `info/` directory contains the population map, `popmap.tsv`, and barcodes
files, `barcodes.laneX.tsv`, for the demonstration dataset.

These scripts and files can be browsed online at (this is the "Source" tab on the
left of this text if viewed online, as of 2017):

    https://bitbucket.org/rochette/rad-seq-genotyping-demo/src

Or downloaded at:

    https://bitbucket.org/rochette/rad-seq-genotyping-demo/get/tip.tar.gz

----------

The full demonstration dataset, including the RAD-seq reads, is available at (8.7Gb):

    http://catchenlab.life.illinois.edu/data/rochette2017_gac_or.tar.gz

----------

_Erratum:_ There is a typo at step 11 in the protocol's PDF: the `>>` operator 
should be used instead of the `>` one. The faulty code replaces the output file 
at each command instead of appending to it, so that the resulting file will be 
identical to `sample_name.rem.2.fq.gz` (which is likely to contain very few 
reads).

Thus the code at step 11 should be:

    cp sample_name.1.fq.gz sample_name.fq.gz
    cat sample_name.2.fq.gz >> sample_name.fq.gz
    cat sample_name.rem.1.fq.gz >> sample_name.fq.gz
    cat sample_name.rem.2.fq.gz >> sample_name.fq.gz
